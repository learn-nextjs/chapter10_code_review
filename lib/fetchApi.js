import axios from 'axios';

/**
 * Under the hood doing axios request to the API endpoint
 *
 * @param {string} endpoint the API endpoint
 * @param {boolean} withToken need token or not / API endpoint protected or not
 * @param {object} options options for request with two properties: method and body, i.e. { method: 'POST', body: { title: 'Task 1', description: 'Lorem ipsum' } }
 * @param {object} head headers for request, e.g. { 'Content-Type' : 'application/json'  }
 * @returns
 */
export async function fetchFrom(endpoint, token = null, options, head) {
  try {
    const headers = token ? { token, ...head } : { ...head };
    const method = options ? options.method : 'GET';
    const data = options ? options.body : '';

    const response = await axios({
      url: `${process.env.REACT_APP_API_BASE_URL}${endpoint}`,
      method,
      headers,
      data,
    });

    return {
      ...response.data,
      status: response.status,
      statusText: response.statusText,
    };
  } catch (error) {
    if (error.response) {
      throw error.response;
    }

    throw error;
  }
}

export const postLogin = (body) => {
  return fetchFrom(
    '/users/login',
    false,
    { method: 'POST', body },
    { 'Content-Type': 'application/json' }
  );
};

export const postRegister = (body) => {
  return fetchFrom(
    '/users/register',
    null,
    { method: 'POST', body },
    { 'Content-Type': 'application/json' }
  );
};

export const getGameList = () => {
  return fetchFrom(`/games`);
};

export const addGamePlayCount = (gameId, token) => {
  return fetchFrom(`/games/playcount/${gameId}`, token, { method: 'PUT' });
};

export const getGameDetail = (gameId) => {
  return fetchFrom(`/games/${gameId}`);
};

export const getAllUsers = (token) => {
  return fetchFrom(`/users`, token);
};

export const getUserDetail = (userId, token) => {
  return fetchFrom(`/users/${userId}`, token);
};

export const updateUserData = (userId, token, body) => {
  return fetchFrom(
    `/users/update/${userId}`,
    token,
    { method: 'PUT', body },
    { 'Content-Type': 'application/json' }
  );
};

export const addUserScore = (gameId, token) => {
  return fetchFrom(`/users/score/game/${gameId}`, token, { method: 'PUT' });
};
