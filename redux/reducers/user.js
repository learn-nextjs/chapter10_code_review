import { USER_LOGIN, USER_REGISTER, USER_LOGOUT } from '../actions/user';

const initialState = {
  isAuthenticated: false,
  id: '',
  name: '',
  username: '',
  email: '',
  score: 0,
  address: '',
  birthdate: '',
  avatar: '',
  createdAt: '',
  updatedAt: '',
  token: '',
};

const userReducer = (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case USER_LOGIN:
      return {
        ...state,
        ...payload.user,
        token: payload.token,
        isAuthenticated: true,
      };
    case USER_REGISTER:
      return {
        ...state,
        ...payload.user,
        token: payload.token,
        isAuthenticated: true,
      };
    case USER_LOGOUT:
      return {
        ...state,
        ...initialState,
      };
    default:
      return state;
  }
};

export default userReducer;
