import { postLogin, postRegister } from "../../lib/fetchApi";

export const USER_LOGIN = "USER_LOGIN";
export const USER_REGISTER = "USER_REGISTER";
export const USER_LOGOUT = "USER_LOGOUT";

// LOGIN
export const userLogin = (body) => async (dispatch) => {
    try {
        const response = await postLogin(body);
        const { user, token } = response;
        localStorage.setItem("token", token);
        dispatch({ type: USER_LOGIN, payload: { user, token } });
    } catch (err) {
        throw err;
    }
};

// REGISTER
export const userRegister = (body) => async (dispatch) => {
    try {
        const response = await postRegister(body);
        const { user, token } = response;
        localStorage.setItem("token", token);
        dispatch({ type: USER_REGISTER, payload: { user, token } });
    } catch (err) {
        throw err;
    }
};

// LOGOUT
export const userLogout = () => (dispatch) => {
    localStorage.removeItem("token");
    dispatch({ type: USER_LOGOUT });
};
