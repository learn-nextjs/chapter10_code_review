import { connect } from 'react-redux';
import { useRouter } from 'next/router';
import Head from 'next/head';
import LoginComp from '../components/Login/LoginComp';
import { Container, Col, Row } from 'react-bootstrap';
import BackgroundComp from '../components/Background/Background';
import styles from '../styles/login.module.css';
import { useEffect, useState } from 'react';
import { userLogin } from '../redux/actions/user';
import { toast, ToastContainer } from 'react-nextjs-toast';

const Login = ({ user, userLogin }) => {
  const router = useRouter();

  // useState
  const [loading, setLoading] = useState(false);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const onSubmit = async (e) => {
    e.preventDefault();

    setLoading(true);

    try {
      await userLogin({ email: email, password: password });

      setLoading(false);
    } catch (err) {
      const {
        data: { errors },
      } = err;

      toast.notify(`${errors[0].msg}`, {
        title: 'Error',
        type: 'error',
      });

      setLoading(false);
    }
  };

  useEffect(() => {
    if (user.isAuthenticated) {
      router.push(`/profile/${user.id}?token=${user.token}`);
    }

    return () => toast.remove();
  }, [user]);

  // Code halaman Login disini
  return (
    <>
      <Head>
        <title>Login</title>
      </Head>

      <ToastContainer align='right' />

      <BackgroundComp background={'url(/img/Log_Reg1.jpg)'}>
        <div>
          <Container className='mt-5'>
            <Row>
              <Col className={`${styles.Log}`} md={7}>
                <p>One Step Closer</p>
              </Col>
              <Col className='App' md={4}>
                <LoginComp
                  setEmail={setEmail}
                  setPassword={setPassword}
                  handleSubmit={onSubmit}
                  loading={loading}
                />
              </Col>
            </Row>
          </Container>
        </div>
      </BackgroundComp>
    </>
  );
};

/**
 * Fungsi ini dipakai untuk melakukan
 * mapping dari redux store ke props component
 */
const mapStateToProps = (state) => ({
  user: state.user,
});

/**
 * Fungsi ini dipakai untuk melakukan
 * mapping action creator (yang sudah didefinisikan di dalam actions)
 * ke props komponen
 */
const mapDispatchToProps = {
  userLogin,
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
