import { connect } from 'react-redux';
import { useRouter } from 'next/router';
import Head from 'next/head';
import RegisterComp from '../components/Register/RegisterComp';
import { Container, Col, Row } from 'react-bootstrap';
import styles from '../styles/register.module.css';
import BackgroundComp from '../components/Background/Background';
import { useEffect, useState } from 'react';
import { userRegister } from '../redux/actions/user';
import { toast, ToastContainer } from 'react-nextjs-toast';

// Code halaman register disini
const Register = ({ user, userRegister }) => {
  const router = useRouter();

  const [loading, setLoading] = useState(false);
  const [name, setName] = useState('');
  const [userName, setUserName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const onSubmit = async (e) => {
    e.preventDefault();

    const registerData = {
      name: name,
      username: userName,
      email: email,
      password: password,
    };

    setLoading(true);

    try {
      await userRegister(registerData);

      setLoading(false);
    } catch (err) {
      const {
        data: { errors },
      } = err;

      toast.notify(`${errors[0].msg}`, {
        title: 'Error',
        type: 'error',
      });

      setLoading(false);
    }
  };

  useEffect(() => {
    if (user.isAuthenticated) {
      router.push(`/profile/${user.id}?token=${user.token}`);
    }

    return () => toast.remove();
  }, [user]);

  return (
    <>
      <Head>
        <title>Register</title>
      </Head>

      <ToastContainer align='right' />

      <BackgroundComp background={'url(/img/Log_Reg1.jpg)'}>
        <div>
          <Container className='mt-5'>
            <Row>
              <Col className={`${styles.Reg} 'register-mt'`} md={7}>
                <p>Join to The Legendary Games</p>
              </Col>
              <Col className='register-mt' md={4}>
                <RegisterComp
                  setName={setName}
                  setUserName={setUserName}
                  setEmail={setEmail}
                  setPassword={setPassword}
                  handleSubmit={onSubmit}
                  loading={loading}
                />
              </Col>
            </Row>
          </Container>
        </div>
      </BackgroundComp>
    </>
  );
};

/**
 * Fungsi ini dipakai untuk melakukan
 * mapping dari redux store ke props component
 */
const mapStateToProps = (state) => ({
  user: state.user,
});

/**
 * Fungsi ini dipakai untuk melakukan
 * mapping action creator (yang sudah didefinisikan di dalam actions)
 * ke props komponen
 */
const mapDispatchToProps = {
  userRegister,
};

export default connect(mapStateToProps, mapDispatchToProps)(Register);
