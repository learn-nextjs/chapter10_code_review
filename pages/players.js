import { useEffect } from 'react';
import { useRouter } from 'next/router';
import { connect } from 'react-redux';
import Head from 'next/head';
import { Col, Container, Button, Row } from 'react-bootstrap';
import BackgroundComp from '../components/Background/Background';
import { getAllUsers } from '../lib/fetchApi';
import styles from '../styles/players.module.css';
import CardComponent from '../components/Card/Card';

export const getServerSideProps = async ({ query }) => {
  const { token } = query;

  try {
    // Request all user data
    const response = await getAllUsers(token);

    return {
      props: {
        players: response.results,
      },
    };
  } catch (error) {
    return {
      redirect: {
        destination: '/login',
        permanent: false,
      },
    };
  }
};

// code halaman Playerlist disini
const Players = ({ user, players }) => {
  const router = useRouter();

  useEffect(() => {
    if (!user.isAuthenticated) {
      router.push(`/login`);
    }
  }, [user]);

  return (
    <>
      <Head>
        <title>Player List</title>
      </Head>
      <BackgroundComp background={'url(/img/list.jpg)'} innerShadow='none'>
        <Container>
          <Row className='mb-4'>
            <Col>
              <h1 className={styles.h1Text}>Player List</h1>
            </Col>
          </Row>
          <Row className='mb-4'>
            <Col>
              <div className={styles.container}>
                {players.length > 0 &&
                  players.map((player, index) => (
                    <CardComponent
                      idP={player.id}
                      username={player.username}
                      email={player.email}
                      score={player.score}
                      token={user.token}
                    />
                  ))}
              </div>
            </Col>
          </Row>
        </Container>
      </BackgroundComp>
    </>
  );
};

/**
 * Fungsi ini dipakai untuk melakukan
 * mapping dari redux store ke props component
 */
const mapStateToProps = (state) => ({
  user: state.user,
});

export default connect(mapStateToProps)(Players);
