import { connect } from 'react-redux';
import { useRouter } from 'next/router';
import Head from 'next/head';
import { Button } from 'react-bootstrap';
import Image from 'next/image';
import styles from '../../styles/profile.module.css';
import { useState, useEffect } from 'react';
import { getUserDetail, updateUserData } from '../../lib/fetchApi'; // 1 import data from external sourve (api, etc)
import { toast, ToastContainer } from 'react-nextjs-toast';

// import component
import BackgroundComp from '../../components/Background/Background';
import Modal from '../../components/Modal/Modal';

// code halaman User Profile disini
const UserProfile = ({ user, postData }) => {
  const router = useRouter();

  const [userProfile, setUserProfile] = useState(postData);
  const [updatedUserData, setUpdatedUserData] = useState({});
  const [showModal, setShowModal] = useState(false);

  useEffect(() => {
    if (!user.isAuthenticated) {
      router.push(`/`);
    }
  }, [user]);

  useEffect(() => {
    return () => toast.remove();
  }, []);

  const handleValueChange = (e) => {
    setUpdatedUserData((prev) => ({
      ...prev,
      [e.target.name]: e.target.value,
    }));
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    const token = localStorage.getItem('token');
    try {
      const response = await updateUserData(user.id, token, updatedUserData);
      setUserProfile(response.result);
      setUpdatedUserData(response.result);
      setShowModal(false);
    } catch (err) {
      const {
        data: { errors },
      } = err;

      toast.notify(`${errors[0].msg}`, {
        title: 'Error',
        type: 'error',
      });
    }
  };

  const handleCloseModal = () => {
    setShowModal(false);
    setUpdatedUserData(userProfile);
  };

  return (
    <>
      <Head>
        <title>User Profile</title>
      </Head>

      <ToastContainer align='right' />

      <BackgroundComp background={`url('/img/yellow-grunge-background2.jpg')`}>
        <div className={styles.container}>
          <div className={styles.main}>
            <div className={styles.contentImg}>
              <Image
                className={styles.userProfileImg}
                src='/img/wibowo.jpg'
                width='300px'
                height='300px'
              />
            </div>
            <div className={styles.contentData}>
              <h1 className={styles.userName}>{userProfile.username}</h1>
              <h1 className={styles.userScore}>SCORE {userProfile.score}</h1>
              <h1 className={styles.userEmail}>{userProfile.email}</h1>
              <h1 className={styles.userAddress}>{userProfile.address}</h1>
              <Button
                variant='danger'
                className={styles.editButton}
                onClick={() => setShowModal(true)}
              >
                EDIT PROFILE
              </Button>
            </div>
          </div>
        </div>
        <Modal
          showModal={showModal}
          updatedUserData={updatedUserData}
          handleCloseModal={handleCloseModal}
          handleSubmit={handleSubmit}
          handleValueChange={handleValueChange}
          modalPlaceHolder={userProfile}
        />
      </BackgroundComp>
    </>
  );
};

/**
 * Fungsi ini dipakai untuk melakukan
 * mapping dari redux store ke props component
 */
const mapStateToProps = (state) => ({
  user: state.user,
});

export default connect(mapStateToProps)(UserProfile);

export async function getServerSideProps({ params, query }) {
  const { token } = query;

  try {
    const res = await getUserDetail(params.userId, token);
    const postData = res.result;

    return {
      props: {
        postData,
      },
    };
  } catch (err) {
    console.error(err.message);

    if (token) {
      return {
        redirect: {
          destination: '/playerlist',
          permanent: false,
        },
      };
    } else {
      return {
        redirect: {
          destination: '/login',
          permanent: false,
        },
      };
    }
  }
}
