import { useEffect, useRef, useState } from "react";
import { connect } from "react-redux";
import { useRouter } from "next/router";
import Head from "next/head";
import Image from "next/image";
import { Card, Col, Container, ListGroup, Row } from "react-bootstrap";
import { addUserScore } from "../../../lib/fetchApi";
import styles from "../../../styles/rps.module.css";

export const getServerSideProps = async ({ params }) => {
    return {
        props: {
            gameId: params.gameId,
        },
    };
};

// code halaman Game Rps disini
const GameRps = ({ user, gameId }) => {
    useEffect(() => {
        if (!user.isAuthenticated) {
            router.push(`/login`);
        }
    }, [user]);

    const router = useRouter();

    const handTypes = [
        {
            name: "rock",
            srcImg: "/img/batu.png",
        },
        {
            name: "paper",
            srcImg: "/img/kertas.png",
        },
        {
            name: "scissor",
            srcImg: "/img/gunting.png",
        },
    ];

    const PLAYER_HAND_REF = {
        rock: useRef(),
        paper: useRef(),
        scissor: useRef(),
    };

    const COMPUTER_HAND_REF = {
        rock: useRef(),
        paper: useRef(),
        scissor: useRef(),
    };

    // const { userId } = useContext(AuthContext);

    // const [userData, setUserData] = useState(null);

    // useEffect(() => {
    //     async function init() {
    //         try {
    //             const response = await getUserData(userId);
    //             setUserData(response.result);
    //         } catch (error) {
    //             console.log({ error });
    //         }
    //     }

    //     init();

    //     // eslint-disable-next-line react-hooks/exhaustive-deps
    // }, []);

    const [gameRound, setGameRound] = useState(0);
    const [playerScore, setPlayerScore] = useState(0);
    const [computerScore, setComputerScore] = useState(0);

    useEffect(() => {
        async function _addUserScore() {
            await addUserScore(gameId, user.token);
        }

        if (playerScore > 0) {
            _addUserScore();
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [playerScore]);

    const [selectedComputerHand, setSelectedComputerHand] = useState(null);
    const [selectedPlayerHand, setSelectedPlayerHand] = useState(null);

    const startGame = (playerHand) => {
        setGameRound((prev) => prev + 1);

        if (selectedPlayerHand) {
            restartGame();
        }

        PLAYER_HAND_REF[playerHand].current.classList.add("selected");
        setSelectedPlayerHand(PLAYER_HAND_REF[playerHand]);

        const computerHand = getComputerHand();
        COMPUTER_HAND_REF[computerHand].current.classList.add("selected");
        setSelectedComputerHand(COMPUTER_HAND_REF[computerHand]);

        getResult(playerHand, computerHand);
    };

    const getComputerHand = () => {
        // generate random number from 0 to 2
        const random = Math.floor(Math.random() * 3);
        const signName = handTypes[random].name;

        return signName;
    };

    const [message, setMessage] = useState("");

    const getResult = (playerHand, computerHand) => {
        let message;
        let _playerScore = 0;
        let _computerScore = 0;

        if (playerHand === computerHand) {
            message = "DRAW";
        } else if (playerHand === "rock") {
            if (computerHand === "scissor") {
                message = `PLAYER WIN`;
                _playerScore += 1;
            } else {
                message = `COMPUTER WIN`;
                _computerScore += 1;
            }
        } else if (playerHand === "paper") {
            if (computerHand === "rock") {
                message = `PLAYER WIN`;
                _playerScore += 1;
            } else {
                message = `COMPUTER WIN`;
                _computerScore += 1;
            }
        } else if (playerHand === "scissor") {
            if (computerHand === "paper") {
                message = `PLAYER WIN`;
                _playerScore += 1;
            } else {
                message = `COMPUTER WIN`;
                _computerScore += 1;
            }
        }

        setPlayerScore((prev) => prev + _playerScore);
        setComputerScore((prev) => prev + _computerScore);

        setMessage(message);
    };

    const restartGame = () => {
        selectedPlayerHand.current.classList.remove("selected");
        selectedComputerHand.current.classList.remove("selected");
        setSelectedPlayerHand(null);
        setSelectedComputerHand(null);
    };

    // if (!userData) return null;

    return (
        <>
            <Head>
                <title>Rock Paper Scissor</title>
            </Head>

            <Container className={styles.game_rps_container}>
                <Row className="mb-4">
                    <Col>
                        <h1 className="text-center">Rock Papper Scissor</h1>
                    </Col>
                </Row>
                <Row>
                    <Col
                        md={{ span: 10, offset: 1 }}
                        lg={{ span: 8, offset: 2 }}
                    >
                        <Card
                            bg="dark"
                            text="warning"
                            className={styles.game_rps_card}
                        >
                            <Card.Body>
                                <Row>
                                    <Col xs={4}>
                                        <h4
                                            className="text-center"
                                            style={{
                                                backgroundColor: "#ffc107",
                                                color: "#343a40",
                                                padding: "10px 5px",
                                                borderRadius: 10,
                                            }}
                                        >
                                            Computer
                                        </h4>
                                        <ListGroup
                                            className={
                                                styles.game_rps_listgroup
                                            }
                                        >
                                            {handTypes.map((hand) => (
                                                <ListGroup.Item
                                                    key={hand.name}
                                                    className={`${styles.game_rps_listitem} game-rps-handsign`}
                                                    ref={
                                                        COMPUTER_HAND_REF[
                                                            hand.name
                                                        ]
                                                    }
                                                >
                                                    <Image
                                                        src={hand.srcImg}
                                                        width={80}
                                                        height={80}
                                                        alt={`computer ${hand.name} hand`}
                                                    />
                                                </ListGroup.Item>
                                            ))}
                                        </ListGroup>
                                    </Col>
                                    <Col xs={4} className="text-center">
                                        <Row
                                            className="align-items-center"
                                            style={{ height: "100%" }}
                                        >
                                            <Col xs={12}>
                                                <Row>
                                                    <Col xs={12}>
                                                        <h5 className="mb-4">
                                                            Round:
                                                        </h5>
                                                    </Col>
                                                    <Col
                                                        xs={{
                                                            span: 6,
                                                            offset: 3,
                                                        }}
                                                    >
                                                        <p className="game-rps-score mb-4">
                                                            {gameRound}
                                                        </p>
                                                    </Col>
                                                    <Col xs={12}>
                                                        <h5 className="mb-4">
                                                            {message}
                                                        </h5>
                                                    </Col>
                                                </Row>
                                            </Col>
                                            <Col xs={12}>
                                                <Row>
                                                    <Col xs={12}>
                                                        <h5 className="mb-4">
                                                            Score:
                                                        </h5>
                                                    </Col>
                                                    <Col xs={6}>
                                                        <p
                                                            className={`game-rps-score
                                                     ${
                                                         computerScore >
                                                         playerScore
                                                             ? "win"
                                                             : ""
                                                     }`}
                                                        >
                                                            {computerScore}
                                                        </p>
                                                    </Col>
                                                    <Col xs={6}>
                                                        <p
                                                            className={`game-rps-score
                                                     ${
                                                         playerScore >
                                                         computerScore
                                                             ? "win"
                                                             : ""
                                                     }`}
                                                        >
                                                            {playerScore}
                                                        </p>
                                                    </Col>
                                                </Row>
                                            </Col>
                                        </Row>
                                    </Col>
                                    <Col xs={4}>
                                        <h4
                                            className="text-center"
                                            style={{
                                                backgroundColor: "#ffc107",
                                                color: "#343a40",
                                                padding: "10px 5px",
                                                borderRadius: 10,
                                            }}
                                        >
                                            {user.username}
                                        </h4>
                                        <ListGroup
                                            className={
                                                styles.game_rps_listgroup
                                            }
                                        >
                                            {handTypes.map((hand) => (
                                                <ListGroup.Item
                                                    key={hand.name}
                                                    className={`${styles.game_rps_listitem} game-rps-handsign player-hand`}
                                                    ref={
                                                        PLAYER_HAND_REF[
                                                            hand.name
                                                        ]
                                                    }
                                                >
                                                    <Image
                                                        src={hand.srcImg}
                                                        width={80}
                                                        height={80}
                                                        alt={`user ${hand.name} hand`}
                                                        onClick={() =>
                                                            startGame(hand.name)
                                                        }
                                                    />
                                                </ListGroup.Item>
                                            ))}
                                        </ListGroup>
                                    </Col>
                                </Row>
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
            </Container>
        </>
    );
};

/**
 * Fungsi ini dipakai untuk melakukan
 * mapping dari redux store ke props component
 */
const mapStateToProps = (state) => ({
    user: state.user,
});

export default connect(mapStateToProps)(GameRps);
