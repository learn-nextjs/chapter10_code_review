import { useEffect, useMemo, useState } from "react";
import { connect } from "react-redux";
import { useRouter } from "next/router";
import Head from "next/head";
import Link from "next/link";
import {
    Alert,
    Button,
    Card,
    Container,
    Col,
    Jumbotron,
    Row,
    Table,
} from "react-bootstrap";
import { generateRandomScore } from "../../../lib";
import { getGameDetail, addGamePlayCount } from "../../../lib/fetchApi";
import { toast, ToastContainer } from "react-nextjs-toast";
import styles from "../../../styles/game-detail.module.css";

export const getServerSideProps = async ({ params }) => {
    try {
        // Request game detail based on game id from server backend
        const response = await getGameDetail(params.gameId);

        return {
            props: {
                gameDetail: response.result,
            },
        };
    } catch (error) {
        return {
            notFound: true,
        };
    }
};

// code halaman Game detail disini
const GameDetail = ({ user, gameDetail }) => {
    const router = useRouter();
    const [loading, setLoading] = useState(false);

    const dummyPlayers = [
        "Farsiko",
        "Frederic",
        "Giodio",
        "Indra",
        "Pranata",
        "Wahyu",
    ];

    const userHasPlayerd = useMemo(() => {
        if (user.isAuthenticated) {
            const userIds = gameDetail.users.map((usr) => usr.id);
            return userIds.indexOf(user.id) !== -1;
        }
        return false;
    }, [user, gameDetail]);

    const goToGamePlay = async () => {
        const { gameId } = router.query;

        setLoading(true);

        try {
            await addGamePlayCount(gameId, user.token);
            setLoading(false);
            router.push(`/games/${gameId}/play`);
        } catch (error) {
            setLoading(false);
            toast.notify(
                `Ooppss!! Can not play the game. ${
                    !user.token ? "You need to login first." : ""
                }`,
                {
                    title: "Error",
                    type: "error",
                }
            );
        }
    };

    useEffect(() => {
        return () => toast.remove();
    }, []);

    return (
        <>
            <Head>
                <title>Game Detail - {gameDetail.title}</title>
            </Head>

            <ToastContainer align="right" />

            <Container className={styles.game_detail_container}>
                <Row className="mb-4">
                    <Col className={styles.game_detail_header}>
                        <h2 className={`text-center ${styles.game_detail_h2}`}>
                            Game Details
                        </h2>
                        <Link href="/games">
                            <Button
                                variant="light"
                                className={styles.game_detail_btn_back}
                            >
                                Back to Game Lists
                            </Button>
                        </Link>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Jumbotron className="text-center mb-3">
                            <h1 className="mb-4">{gameDetail.title}</h1>
                            {gameDetail.link ? (
                                <a
                                    className="btn btn-danger btn-lg mb-4"
                                    href={gameDetail.link}
                                    target="_blank"
                                    rel="noreferrer"
                                >
                                    Play Now
                                </a>
                            ) : (
                                <Button
                                    variant="danger"
                                    size="lg"
                                    className="mb-4"
                                    disabled={loading}
                                    onClick={goToGamePlay}
                                >
                                    {loading ? "Processing..." : "Play Now"}
                                </Button>
                            )}
                            <h4>
                                Played{" "}
                                {gameDetail.link
                                    ? generateRandomScore()
                                    : gameDetail.play_count}{" "}
                                times
                            </h4>
                        </Jumbotron>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Alert variant={userHasPlayerd ? "primary" : "warning"}>
                            <h5 className="text-center">
                                {userHasPlayerd
                                    ? "Pernah Dimainkan"
                                    : "Belum Pernah Dimainkan"}
                            </h5>
                        </Alert>
                    </Col>
                </Row>
                <Row>
                    <Col md={3} className="mb-4">
                        <Card bg="danger" border="dark">
                            <Card.Body className={styles.game_detail_card_body}>
                                <Card.Title className="mb-0 p-4">
                                    <h5 className="mb-0">Player Leaderboard</h5>
                                </Card.Title>
                                <div
                                    className={`pb-3 ${styles.game_detail_card_text}`}
                                >
                                    <Table responsive className="mb-0">
                                        <tbody>
                                            {gameDetail.users &&
                                            gameDetail.users.length > 0
                                                ? gameDetail.users.map(
                                                      (p, index) => (
                                                          <tr key={p.id}>
                                                              <td width="80%">
                                                                  {index + 1}.{" "}
                                                                  {p.username}
                                                              </td>
                                                              <td width="20%">
                                                                  {
                                                                      p.UserGame
                                                                          .score
                                                                  }
                                                              </td>
                                                          </tr>
                                                      )
                                                  )
                                                : dummyPlayers.map(
                                                      (person, index) => (
                                                          <tr key={person}>
                                                              <td width="80%">
                                                                  {index + 1}.{" "}
                                                                  {person}
                                                              </td>
                                                              <td width="20%">
                                                                  {generateRandomScore()}
                                                              </td>
                                                          </tr>
                                                      )
                                                  )}
                                        </tbody>
                                    </Table>
                                </div>
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col md={9} className="mb-4">
                        <Card bg="danger" border="dark">
                            <Card.Body className={styles.game_detail_card_body}>
                                <Card.Title className="mb-0 p-4">
                                    <h5 className="mb-0">Game Description</h5>
                                </Card.Title>
                                <div
                                    className={`p-4 ${styles.game_detail_card_text}`}
                                >
                                    <p>{gameDetail.description}</p>
                                </div>
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
            </Container>
        </>
    );
};

/**
 * Fungsi ini dipakai untuk melakukan
 * mapping dari redux store ke props component
 */
const mapStateToProps = (state) => ({
    user: state.user,
});

export default connect(mapStateToProps)(GameDetail);
