import { useState } from "react";
import Head from "next/head";
import Link from "next/link";
import { Form, Container, Col, Card, Row } from "react-bootstrap";
import { getGameList } from "../../lib/fetchApi";

export const getStaticProps = async () => {
    try {
        // Request all game list
        const response = await getGameList();

        return {
            props: { games: response.results },
        };
    } catch (error) {
        return {
            notFound: true,
        };
    }
};

// code halaman games list disini
const GamesList = ({ games }) => {
    const [searchTerm, setSearchTerm] = useState("");

    return (
        <>
            <Head>
                <title>Game List</title>
            </Head>

            <div style={{ paddingTop: 80 }}>
                <Form className="mb-5">
                    <Form.Row className="align-items-center">
                        <Col md={{ span: 6, offset: 3 }} className="my-1">
                            <Form.Control
                                id="inlineFormInputName"
                                placeholder="Search for Game"
                                onChange={(event) => {
                                    setSearchTerm(event.target.value);
                                }}
                            />
                        </Col>
                    </Form.Row>
                </Form>
                <Container>
                    <Row>
                        {games
                            .filter((val) => {
                                if (searchTerm === "") {
                                    return true;
                                } else if (
                                    val.title
                                        .toLowerCase()
                                        .includes(searchTerm.toLowerCase())
                                ) {
                                    return true;
                                } else {
                                    return false;
                                }
                            })
                            .map((val) => {
                                return (
                                    <Col
                                        key={val.id}
                                        md={4}
                                        lg={3}
                                        className="mb-4"
                                    >
                                        <Card>
                                            <Card.Img
                                                variant="top"
                                                src={val.image}
                                            />
                                            <Card.Body>
                                                <Card.Title>
                                                    {val.title}
                                                </Card.Title>
                                                <Card.Text>
                                                    {val.description.length < 80
                                                        ? val.description
                                                        : `${val.description.substring(
                                                              0,
                                                              79
                                                          )}...`}
                                                </Card.Text>
                                                <Card.Subtitle className="mb-2 text-muted">
                                                    Dimainkan {val.play_count}
                                                </Card.Subtitle>
                                                <Link
                                                    className="btn btn-primary"
                                                    href={`/games/${val.id}`}
                                                >
                                                    <a className="btn btn-primary">
                                                        See Game
                                                    </a>
                                                </Link>
                                            </Card.Body>
                                        </Card>
                                    </Col>
                                );
                            })}
                    </Row>
                </Container>
            </div>
        </>
    );
};

export default GamesList;
