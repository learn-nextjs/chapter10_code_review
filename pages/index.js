import Head from 'next/head';
import Link from 'next/link';
import styles from '../styles/landing.module.css';
import BackgroundComp from '../components/Background/Background';
import Image from 'next/image';

// code halaman Landing page disini
const LandingPage = () => {
  return (
    <>
      <Head>
        <title>Landing Page</title>
      </Head>
      {/* <!-- ========== Jumbotron ========== -->     */}
      <div
        className={`${styles.landing} ${styles.landingBg} jumbotron text-center bg-gradient-dark`}
        style={{ marginBottom: 0 }}
      >
        <div className='content'>
          <h1>PLAY TRADITIONAL GAME</h1>
          <h3>Experience new traditional game play</h3>
          <Link href='/games'>
            <button type='button' className='btn btn-warning'>
              PLAY NOW
            </button>
          </Link>

          <p>THE STORY</p>
          <svg
            width='1em'
            height='1em'
            viewBox='0 0 16 16'
            className='bi bi-caret-down'
            fill='currentColor'
            xmlns='http://www.w3.org/2000/svg'
          >
            <path
              fillRule='evenodd'
              d='M3.204 5L8 10.481 12.796 5H3.204zm-.753.659l4.796 5.48a1 1 0 0 0 1.506 0l4.796-5.48c.566-.647.106-1.659-.753-1.659H3.204a1 1 0 0 0-.753 1.659z'
            />
          </svg>
        </div>
      </div>
      {/* <!-- ========== Jumbotron End ========== --> */}

      {/* <!-- ========== About ========== -->     */}
      <BackgroundComp
        background={`url('/img/landingBG2.jpg')`}
        innerShadow='inset 0px 0px 100px 0px #000'
      >
        <section className={styles.landingAbout}>
          <div className='container'>
            <div className='row'>
              <div className='col-xl-5'>
                <h6>What's so special?</h6>
              </div>
            </div>

            <div className='row'>
              <div className='col-xl-5'>
                <h1>THE GAMES</h1>
              </div>

              <div className='col-xl-7 ml-auto'>
                <div
                  id='carouselExampleIndicators'
                  className='carousel slide navbar-expand-xl'
                  data-ride='carousel'
                >
                  <ol className='carousel-indicators'>
                    <li
                      data-target='#carouselExampleIndicators'
                      data-slide-to='0'
                      className='active'
                    ></li>
                  </ol>

                  <div className='carousel-inner'>
                    <div className='carousel-item active'>
                      <img
                        src='/img/carousel.jpg'
                        className='d-block w-100'
                        alt='rock'
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </BackgroundComp>
      {/* <!-- ========== About End ========== -->     */}

      {/* <!-- ========== System Requirement ========== -->     */}
      <BackgroundComp
        background={`url('/img/landingBG3.jpg')`}
        innerShadow='inset 0px 0px 100px 0px #000'
      >
        <section className={styles.landingSystem} id='system'>
          <div className='container'>
            <div className='row'>
              <div className='col-xl-10'>
                <h4 className='col-xl-6 ml-auto'>
                  Can My Computer Run This Game?
                </h4>
                <h1 className='col-xl-4' style={{ marginTop: '150px' }}>
                  SYSTEM <br /> REQUIREMENTS
                </h1>
              </div>
            </div>

            <div className='row'>
              <div className='col-lg-6'>
                <table
                  className={`${styles.landingTableCustom} table table-responsive`}
                  style={{ marginLeft: '10px' }}
                >
                  <tbody>
                    <tr>
                      <td>
                        <h3>OS:</h3>
                        <p>
                          Windows 7 64-bit only (No OSX support at this time)
                        </p>
                      </td>
                      <td>
                        <h3>PROCESSOR:</h3>
                        <p>
                          Intel Core 2 Duo @ 2.4 GHZ or AMD Athlon X2 @ 2.8 GHZ
                        </p>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <h3>MEMORY:</h3>
                        <p>4 GB RAM</p>
                      </td>
                      <td>
                        <h3>STORAGE:</h3>
                        <p>8 GB available space</p>
                      </td>
                    </tr>
                    <tr>
                      <td colSpan='2'>
                        <h3>GRAPHICS:</h3>
                        <p>
                          NVIDIA GeForce GTX 660 2GB or AMD Radeon HD 7850 2GB
                          DirectX11 (Shader Model 5)
                        </p>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </section>
        ;
      </BackgroundComp>
      {/* <!-- ========== System Requirement End ========== -->     */}

      {/* <!-- ========== Best Player ========== -->     */}
      <BackgroundComp
        background={`url('/img/landingBG4.jpg')`}
        innerShadow='inset 0px 0px 100px 0px #000'
      >
        <section className={styles.landingQuotes}>
          <div className='container'>
            <div className='row'>
              <div className='col-lg-5'>
                <h1
                  style={{
                    marginTop: '250px',
                    fontFamily: `'Anton', sans-serif`,
                    fontSize: '64px',
                  }}
                >
                  TOP SCORES
                </h1>
                <p
                  className='col-lg-11'
                  style={{
                    font: 'normal 400 18px/24px arial, sans-serif',
                    padding: '0px',
                  }}
                >
                  This top score from various games provided on this platform
                </p>
                <Link href='/players'>
                  <button
                    type='button'
                    className='btn btn-warning'
                    href='/players'
                  >
                    see more
                  </button>
                </Link>
              </div>

              <div className='col-lg-6' style={{ marginLeft: '-30px' }}>
                <ul style={{ listStyleType: 'none' }}>
                  <li>
                    <div
                      className={`${styles.landingQuotesCardBg} card text-white mb-4 col-lg-10 offset-lg-3`}
                      style={{ marginTop: '100px' }}
                    >
                      <div className={styles.landingQuotesCardHeader}>
                        <div
                          className={`${styles.landingQuotesCardImg} bg-warning`}
                        >
                          <img
                            className='rounded-circle'
                            src='/img/quotes/evan.jpg'
                            alt='evan'
                          />
                        </div>
                        <p className={styles.landingQuotesPname}>Evan Lahti</p>
                        <p className={styles.landingQuotesPjob}>PC Gamer</p>
                      </div>
                      <div className={styles.landingQuotesCardBody}>
                        <p className={styles.landingQuotesCardText}>
                          “One of my gaming highlights of the year.”
                        </p>
                        <p className={styles.landingQuotesPdate}>
                          October 18, 2018
                        </p>
                      </div>
                    </div>
                  </li>

                  <li>
                    <div
                      className={`${styles.landingQuotesCardBg} card text-white mb-4 col-lg-10`}
                    >
                      <div className={styles.landingQuotesCardHeader}>
                        <div
                          className={`${styles.landingQuotesCardImg} bg-warning`}
                        >
                          <img
                            className='rounded-circle'
                            src='/img/quotes/jada.jpg'
                            alt='Jada'
                          />
                        </div>
                        <p className={styles.landingQuotesPname}>
                          Jada Griffin
                        </p>
                        <p className={styles.landingQuotesPjob}>Nerdreactor</p>
                      </div>
                      <div className={styles.landingQuotesCardBody}>
                        <p className={styles.landingQuotesCardText}>
                          “The next big thing in the world of streaming and
                          survival games.”
                        </p>
                        <p className={styles.landingQuotesPdate}>
                          December 21, 2018
                        </p>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div
                      className={`${styles.landingQuotesCardBg} card text-white mb-4 col-lg-10 offset-lg-2`}
                      style={{ marginBottom: '100px' }}
                    >
                      <div className={styles.landingQuotesCardHeader}>
                        <div
                          className={`${styles.landingQuotesCardImg} bg-warning`}
                        >
                          <img
                            className='rounded-circle'
                            src='/img/quotes/aaron.jpg'
                            alt='Aaron'
                          />
                        </div>

                        <p className={styles.landingQuotesPname}>
                          Aaron Williams
                        </p>
                        <p className={styles.landingQuotesPjob}>Uproxx</p>
                      </div>
                      <div className={styles.landingQuotesCardBody}>
                        <p className={styles.landingQuotesCardText}>
                          “Snoop Dogg Playing The Wildly Entertaining ‘SOS’ Is
                          Ridiculous.”
                        </p>
                        <p className={styles.landingQuotesPdate}>
                          December 24, 2018
                        </p>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </section>
      </BackgroundComp>
      {/* <!-- ========== Best Player End ========== -->     */}
    </>
  );
};

export default LandingPage;
