# FE-App using Next.js

## How to contribute?

Langkah-langkah yang harus dilakukan untuk memulai berkontribusi mengembangkan proyek ini:

1. Clone repositori

    ```
    git clone https://gitlab.com/binar-fullstack-web-bootcamp/chapter-10/tim-2-pushajadulu/fe-app.git
    ```

    Kemudian masuk ke dalam folder repositorinya

    ```
     cd fe-app
    ```

2. Install dependensi

    ```
     npm install
    ```

3. Jalankan aplikasi dengan perintah berikut

    ```
    npm run dev
    ```

    Buka [http://localhost:3001](http://localhost:3001) di browser.

4. Selamat mengembangkan proyek ini. Jangan lupa ikuti git flow yang sudah disepakati ;)

## Project Structure

    .
    ├── components                  # Berisi file-file komponen dari tampilan website
    ├── lib
    │   ├── fetchApi.js             # Kode-kode helper untuk request API backend
    │   └── index.js                # Kode-kode library / bantuan buatan sendiri
    ├── node_modules                # Dependensi proyek
    ├── pages                       # Berisi file-file kode yang akan menjadi halaman-halaman website
    │   ├── api
    │   │   └── hello.js            # Next.js API Routes (SAAT INI ABAIKAN DULU)
    │   ├── games
    │   │   ├── [gameId]
    │   │   │   ├── index.js        # Kode untuk halaman Game Detail di sini (url untuk mengaksesnya: /games/{gameId})
    │   │   │   └── play.js         # Kode untuk halaman Playing Game: Rock Paper Scissor (url untuk mengaksesnya: /games/{gameId}/play)
    │   │   └── index.js            # Kode untuk halaman Game List di sini (url untuk mengaksesnya: /games)
    │   ├── profile
    │   │   └── [userId].js         # Kode untuk halaman User Profile di sini (url untuk mengaksesnya: /profile/{userId})
    │   ├── _app.js                 # (SAAT INI ABAIKAN DULU)
    │   ├── index.js                # Kode untuk halaman Landing Page di sini (url untuk mengaksesnya: /)
    │   ├── login.js                # Kode untuk halaman Login di sini (url untuk mengaksesnya: /login)
    │   ├── players.js              # Kode untuk halaman Player List di sini (url untuk mengaksesnya: /players)
    │   └── register.js             # Kode untuk halaman Register di sini (url untuk mengaksesnya: /register)
    ├── public
    │   └── img                     # Folder untuk menyimpan file-file gambar
    ├── styles                      # Folder untuk menyimpan file-file css
    │   ├── game-detail.module.css  # File css untuk halaman Game Detail
    │   ├── games.module.css        # File css untuk halaman Game List
    │   ├── global.css              # File css untuk global css (diterapkan di semua halaman)
    │   ├── landing.module.css      # File css untuk halaman Landing Page
    │   ├── login.module.css        # File css untuk halaman Login
    │   ├── players.module.css      # File css untuk halaman Player List
    │   ├── profile.module.css      # File css untuk halaman User Profile
    │   ├── register.module.css     # File css untuk halaman Register
    │   └── rps.module.css          # File css untuk halaman Game Rock Paper Scissor
    ├── .env.development            # Environment variables for development stage
    ├── .gitignore
    ├── package-lock.json
    ├── package.json
    └── README.md                   # File README, jangan lupa dibaca ;)
