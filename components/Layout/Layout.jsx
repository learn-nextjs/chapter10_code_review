import Navbar from '../Navbar/Navbar';

export default function Layout({ children }) {
  return (
    <div>
      <Navbar />
      <div className='mt-4'>{children}</div>
    </div>
  );
}
