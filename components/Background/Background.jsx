// import styling here

const BackgroundComp = (props) => {
  const bg = {
    backgroundImage: `${props.background}`,
    backgroundSize: 'cover',
    backgroundPosition: 'center center',
    backgroundRepeat: 'no-repeat',
    minHeight: '100vh',
    minWidth: '100vw',
    boxShadow: props.innerShadow,
  };

  return <div style={bg}>{props.children}</div>;
};

export default BackgroundComp;
