// import component here
import ButtonComp from "../Button/Button";

// import styling here
import styles from "./Register.module.css";
import { FormControl } from "react-bootstrap";

const RegisterComp = (props) => {
    return (
        <>
            <div className={`${styles.formReg}`}>
                <h2 className={`${styles.h2}`}>REGISTER</h2>
                <form>
                    <FormControl
                        type="text"
                        name="name"
                        placeholder="name"
                        className="my-3"
                        onChange={(e) => props.setName(e.target.value)}
                    />
                    <FormControl
                        type="text"
                        name="user name"
                        placeholder="user name"
                        className="my-3"
                        onChange={(e) => props.setUserName(e.target.value)}
                    />
                    <FormControl
                        type="email"
                        name="email"
                        placeholder="email"
                        className="form-control my-3"
                        onChange={(e) => props.setEmail(e.target.value)}
                    />
                    <FormControl
                        type="password"
                        name="password"
                        placeholder="password"
                        className="my-3"
                        onChange={(e) => props.setPassword(e.target.value)}
                    />
                    <ButtonComp
                        variant="danger"
                        className="w-100 mt-4"
                        label="Sign Up"
                        handleClick={props.handleSubmit}
                        loading={props.loading}
                    />
                </form>
            </div>
        </>
    );
};

export default RegisterComp;
