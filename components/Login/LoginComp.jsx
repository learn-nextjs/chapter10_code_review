import Link from 'next/link';
// import component here
import ButtonComp from '../Button/Button';

// import styling here
import styles from './Login.module.css';
import { FormControl } from 'react-bootstrap';

const LoginComp = (props) => {
  return (
    <>
      <div className={`${styles.formLog}`}>
        <h2 className={`${styles.h2}`}>LOGIN</h2>
        <form>
          <FormControl
            type='email'
            name='email'
            placeholder='email'
            className='form-control my-3 mt-4'
            onChange={(e) => props.setEmail(e.target.value)}
          />
          <FormControl
            type='password'
            name='password'
            placeholder='password'
            className='my-3'
            onChange={(e) => props.setPassword(e.target.value)}
          />
          <ButtonComp
            variant='danger'
            className='w-100 mt-5'
            label='Log In'
            handleClick={props.handleSubmit}
            loading={props.loading}
          />
        </form>
      </div>
    </>
  );
};

export default LoginComp;
