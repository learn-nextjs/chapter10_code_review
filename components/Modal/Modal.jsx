import { Button, Modal, Form } from 'react-bootstrap';

const ModalComponent = (props) => {
  return (
    <>
      <Modal show={props.showModal} onHide={() => props.handleCloseModal()}>
        <Modal.Header closeButton>
          <Modal.Title>Update User Data</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group>
              <Form.Label>Name</Form.Label>
              <Form.Control
                type='text'
                name='name'
                placeholder={props.modalPlaceHolder.name}
                value={props.updatedUserData.name}
                onChange={(e) => props.handleValueChange(e)}
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Username</Form.Label>
              <Form.Control
                type='text'
                name='username'
                placeholder={props.modalPlaceHolder.username}
                value={props.updatedUserData.username}
                onChange={(e) => props.handleValueChange(e)}
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Email</Form.Label>
              <Form.Control
                type='email'
                name='email'
                placeholder={props.modalPlaceHolder.email}
                value={props.updatedUserData.email}
                onChange={(e) => props.handleValueChange(e)}
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Address</Form.Label>
              <Form.Control
                type='text'
                name='address'
                placeholder={props.modalPlaceHolder.address}
                value={props.updatedUserData.address}
                onChange={(e) => props.handleValueChange(e)}
              />
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant='secondary' onClick={() => props.handleCloseModal()}>
            Cancel
          </Button>
          <Button variant='success' onClick={(e) => props.handleSubmit(e)}>
            Submit
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default ModalComponent;
