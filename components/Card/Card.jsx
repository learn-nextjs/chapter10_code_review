import { Button } from 'react-bootstrap';
import styles from './Card.module.css';
import Image from 'next/Image';
import Link from 'next/link';

const CardComponent = (props) => {
  return (
    <>
      <div className={styles.content} key={props.id}>
        <div>
          <Image
            className={styles.contentImg}
            src='/img/avatar.jpg'
            height='100px'
            width='100px'
          />
        </div>
        <div className={styles.contentText}>
          <h1 className={styles.contentTitle}>{props.username}</h1>
          <p className={styles.contentBody}>Score : {props.score}</p>
          <Link
            href={`/profile/${props.idP}${
              props.token ? `?token=${props.token}` : ''
            }`}
          >
            <Button variant='danger'>See Details</Button>
          </Link>
        </div>
      </div>
    </>
  );
};

export default CardComponent;
