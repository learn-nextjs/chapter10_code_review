import { connect } from 'react-redux';
import Link from 'next/link';
import { Navbar, Nav } from 'react-bootstrap';
import { userLogout } from '../../redux/actions/user';
import styles from './Navbar.module.css';

// code komponen navbar disini
const NavComponent = ({ user, userLogout }) => {
  const NavLogOut = (
    <Navbar collapseOnSelect expand='lg' bg='dark' fixed='top' variant='dark'>
      <Navbar.Brand
        className={styles.logo}
        href={`/${user.token ? `?token=${user.token}` : ''}`}
      >
        Team PushAjaDulu
      </Navbar.Brand>
      <Navbar.Toggle aria-controls='responsive-navbar-nav' />
      <Navbar.Collapse id='responsive-navbar-nav'>
        <Nav className='ml-auto'>
          <Link href='/games'>
            <a className='nav-link mx-4'>Game List</a>
          </Link>
          <Link href='/login'>
            <a className='nav-link mx-4'>Login</a>
          </Link>
          <Link href='/register'>
            <a className='nav-link mx-4'>Register</a>
          </Link>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );

  const NavLogIn = (
    <Navbar
      collapseOnSelect
      scrolling
      dark
      expand='lg'
      fixed='top'
      bg='dark'
      variant='dark'
    >
      <Navbar.Brand className={styles.logo}>
        Hello {user.name} ({user.score})
      </Navbar.Brand>
      <Navbar.Toggle aria-controls='responsive-navbar-nav' />
      <Navbar.Collapse id='responsive-navbar-nav'>
        <Nav className='mx-auto'>
          <Link
            href={`/profile/${user.id}${
              user.token ? `?token=${user.token}` : ''
            }`}
          >
            <a className='nav-link mx-4'>Profile</a>
          </Link>
          <Link href={`/players${user.token ? `?token=${user.token}` : ''}`}>
            <a className='nav-link mx-4'>Player List</a>
          </Link>
          <Link href='/games'>
            <a className='nav-link mx-4'>Game List</a>
          </Link>
        </Nav>
        <Nav>
          <a className='nav-link mx-4' onClick={userLogout} href='/'>
            Logout
          </a>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );

  return <>{user.isAuthenticated ? NavLogIn : NavLogOut}</>;
};

/**
 * Fungsi ini dipakai untuk melakukan
 * mapping dari redux store ke props component
 */
const mapStateToProps = (state) => ({
  user: state.user,
});

/**
 * Fungsi ini dipakai untuk melakukan
 * mapping action creator (yang sudah didefinisikan di dalam actions)
 * ke props komponen
 */
const mapDispatchToProps = {
  userLogout,
};

export default connect(mapStateToProps, mapDispatchToProps)(NavComponent);
