import { Button } from "react-bootstrap";

const ButtonComp = (props) => {
    return (
        <>
            <Button
                variant={props.variant}
                className={props.className}
                onClick={props.handleClick}
                href={props.href}
                disabled={props.loading}
            >
                {props.loading ? "Processing..." : props.label}
            </Button>
        </>
    );
};

export default ButtonComp;
